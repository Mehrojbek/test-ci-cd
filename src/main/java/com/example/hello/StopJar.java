//package com.example.hello;
//
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.util.ArrayList;
//import java.util.List;
//
//public class StopJar {
//    public static void main(String[] args) {
//        try {
//
//            List<Integer> pids = new ArrayList<>();
//
//            Runtime rt = Runtime.getRuntime();
//            Process process = rt.exec("lsof -ti tcp:8990");
//
//            //QAYTGAN JAVOB NI O'QIB OLAMIZ
//            final BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
//            String line = "";
//            while ((line = reader.readLine()) != null) {
//                getPidsFromString(line, pids);
//            }
//
//            //KILL QILISH UCHUN COMMAND
//            String killCommand = "kill -9 ";
//
//            //HAMMA PID LAR NI KILL QILAMIZ
//            if (!pids.isEmpty())
//                for (Integer pid : pids)
//                    System.out.println(Runtime.getRuntime().exec(killCommand + pid));
//
//            System.out.println(rt.exec("git pull origin master"));
//
//            System.out.println(rt.exec("mvn clean install"));
//
//            Runtime rtForRun = Runtime.getRuntime();
//
//            System.out.println(rtForRun.exec("java -jar /home/user/erp/app-academic-content/target/academic-content.jar"));
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private static void getPidsFromString(String line, List<Integer> pids) {
//        try {
//            int pid = Integer.parseInt(line);
//            pids.add(pid);
//        } catch (Exception exception) {
//            exception.printStackTrace();
//        }
//    }
//}
//
