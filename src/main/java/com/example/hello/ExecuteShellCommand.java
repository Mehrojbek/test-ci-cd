//package com.example.hello;
//
//import java.io.BufferedReader;
//import java.io.InputStreamReader;
//
//public class ExecuteShellCommand {
//
//    public static void main(final String[] args) {
//
//        final ExecuteShellCommand obj = new ExecuteShellCommand();
//        final String ping = "ping -c 3 google.com";
//        final String lsof = "lsof | wc -l";
//        final String output = obj.executeCommand(ping);
//        System.out.println(output);
//    }
//
//    private String executeCommand(final String command) {
//        final StringBuffer output = new StringBuffer();
//        final Process p;
//        try {
//            p = Runtime.getRuntime().exec(command);
//            final int waitForStatus = p.waitFor();
//            System.out.println("waitForStatus=" + waitForStatus);
//            final BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
//            String line = "";
//            while ((line = reader.readLine()) != null) {
//                output.append(line + "\n");
//            }
//        } catch (final Exception e) {
//            e.printStackTrace();
//        }
//        return output.toString();
//    }
//}
