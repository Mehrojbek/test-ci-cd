
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class CICDExecutor {

    public static void main(String[] args) {
        try {

            List<Integer> pids = new ArrayList<>();

            System.out.println("nima gapeee");

            Runtime rt = Runtime.getRuntime();
            Process process = rt.exec("lsof -ti tcp:9191");



            //QAYTGAN JAVOB NI O'QIB OLAMIZ
            final BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line = "";
            while ((line = reader.readLine()) != null) {
                getPidsFromString(line, pids);
            }

            //KILL QILISH UCHUN COMMAND
            String killCommand = "kill -9 ";

            //HAMMA PID LAR NI KILL QILAMIZ
            if (!pids.isEmpty())
                for (Integer pid : pids)
                    System.out.println(Runtime.getRuntime().exec(killCommand + pid));

            Runtime runtimeForPull = Runtime.getRuntime();
            System.out.println("pulll start");
            System.out.println(runtimeForPull.exec("git -C /home/user/erp/test-ci-cd/ pull origin main"));
            Thread.sleep(5000);
            System.out.println("pulll end");

            Runtime runtimeForClean = Runtime.getRuntime();
            System.out.println("clean install start");
            System.out.println(runtimeForClean.exec("mvn -f /home/user/erp/test-ci-cd/pom.xml clean install"));
            Thread.sleep(6000);
            System.out.println("clean install end");

            Runtime rtForRun = Runtime.getRuntime();
            System.out.println("run start");
            System.out.println(rtForRun.exec("java -jar /home/user/erp/test-ci-cd/target/hello.jar"));
            System.out.println("run end");

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void getPidsFromString(String line, List<Integer> pids) {
        try {
            int pid = Integer.parseInt(line);
            pids.add(pid);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

}
